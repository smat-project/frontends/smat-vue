module.exports = {
  // NOTE 2022-03-23 mix - this doesn't seem needed any more
  // devServer: {
  //   proxy: 'http://localhost:80/'
  // },
  publicPath:
    process.env.CI_ENVIRONMENT_NAME === 'staging'
      ? '/frontends/' + process.env.CI_PROJECT_NAME + '/'
      : '/',
}
